
@include('layouts/header')

<!-- Start Content-->
<div class="container-fluid">   

    <div class="row">
        <div class="col-md-12">
            <div class="card mt-4">
                <div class="card-body">

                    @yield('container')

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div><!-- end row -->
</div> <!-- container -->

@include('layouts/footer')