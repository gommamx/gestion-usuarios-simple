## Instalación

PHP version 7.0.15 (cli) (built: Jan 23 2017 15:06:38)
Composer version 1.8.4 2019-02-11 10:52:10
Laravel Framework 5.5.45

```console
foo@bar:~$ composer install
foo@bar:~$ composer dump-autoload
```
* Generar .env con datos de BD para gestionar migraciones y seeds

```console
foo@bar:~$ php artisan key:generate
foo@bar:~$ php artisan migrate --seed
foo@bar:~$ php artisan serv
```
¡Happy!